#ifndef  __SETUP_CHAIN_H
#define __SETUP_CHAIN_H

#include "TString.h"
/**
* @brief Stores paths of ROOT files into a vector.
*
* @param [in] path_to_list Path to a text file, that contains the individual full paths of the ROOT files in each line.
*
* @return Vector of paths to files.
*/
std::vector <TString> getListOfFiles(TString path_to_list);

/**
* @brief Combines TTress into a TChain object.
*
* @param [in] files_list All ROOT files that should be combined.
* @param [in] tree_name Name of the TTree that can be found in all ROOT files.
*
* @return TChain to loop over the combined TTrees.
*/
TChain * getChain(std::vector <TString> files_list, TString tree_name);

/**
 * @brief Combines TTress into a TChain object, gets the inputfilenames using a wildcard symbol, like output*.root,
 * if there are files liek output1.root, output2.root and output3.root to be combined.
 *
 * @param [in] file_name_reg_ex Name of the filse that should be combined using a '*' wildcard.
 * @param [in] tree_name Name of the TTree that can be found in all ROOT files.
 *
 * @return TChain to loop over the combined TTrees.
 */
TChain * getChain(TString file_name_reg_ex, TString tree_name);

#endif // __SETUP_CHAIN_H
