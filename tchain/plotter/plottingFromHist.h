
#include "TEfficiency.h"
#include "TH1F.h"
#include "TString.h"

/**
* @brief Plots the RpT variable for ITk and ITk+HGTD case, with HS and PU jets separated.
*
* @param [in]  hist_hs       Histogram with the RpT distribution of the HS jets.
* @param [in]  hist_pu       Histogram with the RpT distribution of the PU jets.
* @param [in]  hist_hs_hgtd  Histogram with the RpT distribution of the HS jets including HGTD.
* @param [in]  hist_pu_hgtd  Histogram with the RpT distribution of the HS jets including HGTD.
*
*/
void plotRptHGTD(TString dataset_name, TH1F *hist_hs, TH1F *hist_pu, TH1F *hist_hs_hgtd, TH1F *hist_pu_hgtd);


/**
* @brief Plots the RpT variable for ITk and ITk+HGTD case, with HS and PU jets separated.
*
* @param [in]  hist_hs       Histogram with the RpT distribution of the HS jets.
* @param [in]  hist_pu       Histogram with the RpT distribution of the PU jets.
* @param [in]  hist_hs_hgtd  Histogram with the RpT distribution of the HS jets including HGTD.
* @param [in]  hist_pu_hgtd  Histogram with the RpT distribution of the HS jets including HGTD.
*
*/
void plotROCcurve(TString dataset_name, TEfficiency *eff_hs, TEfficiency *eff_pu, TEfficiency *eff_hs_hgtd, TEfficiency *eff_pu_hgtd);
