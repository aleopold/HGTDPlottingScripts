//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Feb 28 17:31:18 2018 by ROOT version 6.10/04
// from TChain Track/
//////////////////////////////////////////////////////////

#ifndef TrackSelector_h
#define TrackSelector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector
class TH1F;

class TrackSelector : public TSelector {
public :
  TTreeReader     fReader;  //!the tree reader
  TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

  TString m_dataset_name = "VBFH125_bb, mu=200";
  TH1F *hist_track_time = nullptr;
  TH1F *hist_pt = nullptr;
  TH1F *hist_n_hits = nullptr;
  Double_t m_n_hits_pt_cut = 1.0; //in GeV

  // Readers to access the data (delete the ones you do not need).
  // TTreeReaderValue<Int_t> m_eventnumber = {fReader, "m_eventnumber"};
  // TTreeReaderValue<Int_t> m_runnumber = {fReader, "m_runnumber"};
  // TTreeReaderValue<Double_t> track_eta = {fReader, "track_eta"};
  TTreeReaderValue<Double_t> track_R = {fReader, "track_R"};
  // TTreeReaderValue<Double_t> m_track_x = {fReader, "m_track_x"};
  // TTreeReaderValue<Double_t> m_track_y = {fReader, "m_track_y"};
  // TTreeReaderValue<Double_t> m_sigma_t = {fReader, "m_sigma_t"};
  TTreeReaderValue<Double_t> track_Pt = {fReader, "track_Pt"};
  // TTreeReaderValue<Double_t> m_track_energy = {fReader, "m_track_energy"};
  TTreeReaderValue<Int_t> track_hit = {fReader, "track_hit"};
  TTreeReaderValue<Double_t> track_time = {fReader, "track_time"};
  // TTreeReaderValue<Double_t> track_time_res = {fReader, "track_time_res"};
  // TTreeReaderValue<Double_t> m_track_time_res_reco = {fReader, "m_track_time_res_reco"};
  // TTreeReaderValue<Double_t> m_used_vertex_time = {fReader, "m_used_vertex_time"};
  // TTreeReaderValue<Double_t> m_z0_parameter = {fReader, "m_z0_parameter"};
  // TTreeReaderValue<Double_t> nevent = {fReader, "nevent"};


  TrackSelector(TTree * /*tree*/ =0) { }
  virtual ~TrackSelector() { }
  virtual Int_t   Version() const { return 2; }
  virtual void    Begin(TTree *tree);
  virtual void    SlaveBegin(TTree *tree);
  virtual void    Init(TTree *tree);
  virtual Bool_t  Notify();
  virtual Bool_t  Process(Long64_t entry);
  virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
  virtual void    SetOption(const char *option) { fOption = option; }
  virtual void    SetObject(TObject *obj) { fObject = obj; }
  virtual void    SetInputList(TList *input) { fInput = input; }
  virtual TList  *GetOutputList() const { return fOutput; }
  virtual void    SlaveTerminate();
  virtual void    Terminate();

  TCanvas *drawNumberOfTrackHits(TH1F* histo);


  ClassDef(TrackSelector,0);
};

#endif

#ifdef TrackSelector_cxx
void TrackSelector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t TrackSelector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef TrackSelector_cxx
