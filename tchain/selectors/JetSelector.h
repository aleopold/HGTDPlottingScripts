//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Feb 28 20:58:41 2018 by ROOT version 6.10/04
// from TChain jet_tree/
//////////////////////////////////////////////////////////

#ifndef JetSelector_h
#define JetSelector_h

#include <TROOT.h>
#include <TChain.h>
#include <TEfficiency.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector


class JetSelector : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   //for rpt combined plot
   Int_t rpt_n_bins = 50;
   Double_t rpt_max_rpt = 1.5;
   TH1F *hist_rpt_hs = nullptr;
   TH1F *hist_rpt_pu = nullptr;
   TH1F *hist_rpt_hs_hgtd = nullptr;
   TH1F *hist_rpt_pu_hgtd = nullptr;
   //eta distribution of hs and pu jets
   Double_t eta_pt_min = 20.; //in GeV
   Double_t eta_pt_max = 40.;
   TH1F *hist_eta_hs = nullptr;
   TH1F *hist_eta_pu = nullptr;
   //efficiencies of PU events
   vector <Double_t> eff_rpt_cut;
   TEfficiency* hist_eff_hs = nullptr;
   TEfficiency* hist_eff_pu = nullptr;
   TEfficiency* hist_eff_hs_hgtd = nullptr;
   TEfficiency* hist_eff_pu_hgtd = nullptr;

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Int_t> m_eventnumber = {fReader, "m_eventnumber"};
   TTreeReaderValue<Int_t> m_runnumber = {fReader, "m_runnumber"};
   TTreeReaderValue<Double_t> m_jet_pt = {fReader, "m_jet_pt"};
   TTreeReaderValue<Double_t> m_jet_e = {fReader, "m_jet_e"};
   TTreeReaderValue<Double_t> m_jet_eta = {fReader, "m_jet_eta"};
   TTreeReaderValue<Double_t> m_jet_phi = {fReader, "m_jet_phi"};
   TTreeReaderValue<Double_t> m_jet_smallest_deltaR_to_truthcone = {fReader, "m_jet_smallest_deltaR_to_truthcone"};
   TTreeReaderValue<Double_t> m_jet_Rpt = {fReader, "m_jet_Rpt"};
   TTreeReaderValue<Double_t> m_jet_Rpt_hgtd = {fReader, "m_jet_Rpt_hgtd"};
   // TTreeReaderValue<Double_t> m_jet_Rpt_hgtd_truth = {fReader, "m_jet_Rpt_hgtd_truth"};
   // TTreeReaderValue<Double_t> m_jet_corrJVF = {fReader, "m_jet_corrJVF"};
   // TTreeReaderValue<Double_t> m_jet_corrJVF_hgtd = {fReader, "m_jet_corrJVF_hgtd"};
   // TTreeReaderValue<Double_t> m_jet_JVF = {fReader, "m_jet_JVF"};
   // TTreeReaderValue<Double_t> m_jet_JVF_hgtd = {fReader, "m_jet_JVF_hgtd"};
   // TTreeReaderValue<Int_t> m_number_of_tracks_assoc_to_jet = {fReader, "m_number_of_tracks_assoc_to_jet"};


   JetSelector(TTree * /*tree*/ =0) { }
   virtual ~JetSelector() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(JetSelector,0);

};

#endif

#ifdef JetSelector_cxx
void JetSelector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t JetSelector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef JetSelector_cxx
