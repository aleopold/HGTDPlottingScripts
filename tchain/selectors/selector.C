#define __SELECTOR_C

#include "selector.h"
#include "TFile.h"

void Selector::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Associate the reader and the tree
  tr_jet_tree.SetTree(tree);
}

void Selector::SlaveBegin(TTree *tree)
{
  // SlaveBegin() is a good place to create histograms.
  // For PROOF, this is called for each worker.
  // The TTree* is there for backward compatibility; e.g. PROOF passes 0.

  hist_pt = new TH1F("hist", ";p_{T};events", 200, 0, 6000);
  hist_rpt  = new TH1F("hist", ";RpT;events", 50, 0, 1.5);
  // Add to output list (needed for PROOF)
  GetOutputList()->Add(hist_pt);
  GetOutputList()->Add(hist_rpt);
}

Bool_t Selector::Process(Long64_t entry)
{
  // The Process() function is called for each entry in the tree to be
  // processed. The entry argument specifies which entry in the currently
  // loaded tree is to be processed.
  // It can be passed to either EventSelector::GetEntry() or TBranch::GetEntry()
  // to read either all or the required parts of the TTree.
  //
  // This function should contain the "body" of the analysis: select relevant
  // tree entries, run algorithms on the tree entry and typically fill histograms.

  // *** 1. *** Tell the reader to load the data for this entry:
  tr_jet_tree.SetEntry(entry);

  // *** 2. *** Do the actual analysis
  hist_pt->Fill(*rv_pt);
  hist_rpt->Fill(*rv_rpt);

  return kTRUE;
}

void Selector::Terminate()
{
  // The Terminate() function is the last function to be called during the
  // analysis of a tree with a selector. It always runs on the client, it can
  // be used to present the results graphically or save the results to file.
  TFile *output_file = new TFile("output.root", "RECREATE");
  hist_pt->Write();
  hist_rpt->Write();
  output_file->Close();
}
