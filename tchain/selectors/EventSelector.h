//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Feb 28 20:37:42 2018 by ROOT version 6.10/04
// from TChain OncePerEvent/
//////////////////////////////////////////////////////////

#ifndef EventSelector_h
#define EventSelector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector


class EventSelector : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   TH1F *hist_used_tvx = nullptr;
   TH1F *hist_n_reco_vertices = nullptr;

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Int_t> m_eventnumber = {fReader, "m_eventnumber"};
   TTreeReaderValue<Int_t> m_runnumber = {fReader, "m_runnumber"};
   TTreeReaderValue<Int_t> m_n_associated_tracks = {fReader, "m_n_associated_tracks"};
   TTreeReaderValue<Double_t> m_truth_vertex_time = {fReader, "m_truth_vertex_time"};
   TTreeReaderValue<Double_t> m_weighted_vertex_time = {fReader, "m_weighted_vertex_time"}; //for step 1.6
   // TTreeReaderValue<Double_t> m_weighted_vertex_time_all = {fReader, "m_weighted_vertex_time_all"};
   TTreeReaderValue<Double_t> m_vertex_z = {fReader, "m_vertex_z"};
   TTreeReaderValue<Int_t> m_n_tracks_in_event = {fReader, "m_n_tracks_in_event"};
   TTreeReaderValue<Int_t> m_n_vertices = {fReader, "m_n_vertices"};
   // TTreeReaderValue<Double_t> m_vertex_time_leading = {fReader, "m_vertex_time_leading"}; //for root files after dz implementation


   EventSelector(TTree * /*tree*/ =0) { }
   virtual ~EventSelector() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(EventSelector,0);

};

#endif

#ifdef EventSelector_cxx
void EventSelector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t EventSelector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef EventSelector_cxx
