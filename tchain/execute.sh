find "."  -type f -name "._*" -delete

#path="/data/atlas/aleopold/hgtd/user.aleopold.hgtd_bbar_mu200_DeltaZandLeadingTime.2018_02_27_1750_HGTDHitAnalysis"
#ls -d  $path/* > INPUT_LIST_lpnatlas.txt

rm *.so
rm *.pcm
rm *.d

#root -l -q -b mainScript.C
root -l -q -b mainScript.C'("selectors/TrackSelector.C+", "Track")' #step 1.6
# root -l -q -b mainScript.C'("selectors/TrackSelector.C+", "track_tree")' #step 2.2
root -l -q -b mainScript.C'("selectors/JetSelector.C+", "jet_tree")'
#root -l -q -b mainScript.C'("selectors/LarHitSelector.C+", "lar_hits")'
root -l -q -b mainScript.C'("selectors/EventSelector.C+", "OncePerEvent")' #step 1.6
# root -l -q -b mainScript.C'("selectors/EventSelector.C+", "once_per_event_tree")' #step 2.2
