#ifndef  __RPT_HELPERS_H
#define __RPT_HELPERS_H

/**
     * @brief  Plots the RpT variable in the range from 0 to 2. The histograms are normalized to 1.
     *
     * @param [in] with_hgtd If set to true, the HGTD information is taken into account for separating true from fake jets.
     *
     * @return Return 0 if ran correctly. Saves the drawn canvas into a file, in pdf and png format.
     */
int plotRpt(bool with_hgtd, Double_t eta_low_cut, Double_t eta_high_cut, TString root_file, TString dataset_name);

int plotRptHGTD(TString root_file, TString dataset_name, bool scale);

int plotZ0parameter(TString root_file, TString dataset_name);

int plotVertexTime(TString root_file, TString dataset_name, TString which_vertex = "truthVertex");

int plotTimingResolutionFromTool(TString root_file, TString dataset_name);

int trackTimeDistribution(TString root_file, TString dataset_name);

int absTimeDifference(TString root_file, TString dataset_name, TString which_vertex);

int plotNumberOfTracks(TString root_file, TString dataset_name);

//plots the eta distribution of PU and HS jets in one histogram
int plotEtaOfPUandHS(TString root_file, TString dataset_name, Double_t pt_min, Double_t pt_max);

int plotROCcurve(TString root_file, TString dataset_name);


#endif // __RPT_HELPERS_H
