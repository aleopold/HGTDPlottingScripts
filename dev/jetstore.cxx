#define JetSelector_cxx
// The class definition in JetSelector.h has been generated automatically
// by the ROOT utility TTree::MakeSelector(). This class is derived
// from the ROOT class TSelector. For more information on the TSelector
// framework see $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.


// The following methods are defined in this file:
//    Begin():        called every time a loop on the tree starts,
//                    a convenient place to create your histograms.
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    SlaveTerminate: called at the end of the loop on the tree, when on PROOF
//                    called only on the slave servers.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//
// root> T->Process("JetSelector.C")
// root> T->Process("JetSelector.C","some options")
// root> T->Process("JetSelector.C+")
//


#include "JetSelector.h"
#include <TCanvas.h>
#include <TH2.h>
#include <TLegend.h>
#include <TStyle.h>
#include "style/AtlasUtils.C"

void JetSelector::Begin(TTree * /*tree*/)
{
   // The Begin() function is called at the start of the query.
   // When running with PROOF Begin() is only called on the client.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();
}

void JetSelector::SlaveBegin(TTree * /*tree*/)
{
   // The SlaveBegin() function is called after the Begin() function.
   // When running with PROOF SlaveBegin() is called on each slave server.
   // The tree argument is deprecated (on PROOF 0 is passed).

   TString option = GetOption();

   // TString rpt_title = Form(";R_{pT}; Fraction of events / %.2f", m_rpt_rpt_cut/(Double_t) m_rpt_n_bins);
   // TH1F *hist_rpt_hs = new TH1F("RpT_HS", rpt_title, m_rpt_n_bins, 0, m_rpt_rpt_cut);
   TH1F *hist_rpt_hs = new TH1F("RpT_HS", ";x;y" , 50, 0, 1.5);
   // TH1F *hist_rpt_pu = new TH1F("RpT_PU", rpt_title, m_rpt_n_bins, 0, m_rpt_rpt_cut);
   // TH1F *hist_rpt_hs_hgtd = new TH1F("RpT_HS_HGTD", rpt_title, m_rpt_n_bins, 0, m_rpt_rpt_cut);
   // TH1F *hist_rpt_pu_hgtd = new TH1F("RpT_PU_HGTD", rpt_title, m_rpt_n_bins, 0, m_rpt_rpt_cut);


   GetOutputList()->Add(hist_rpt_hs);
   // GetOutputList()->Add(hist_rpt_pu);
   // GetOutputList()->Add(hist_rpt_hs_hgtd);
   // GetOutputList()->Add(hist_rpt_pu_hgtd);
}

Bool_t JetSelector::Process(Long64_t entry)
{
  // The Process() function is called for each entry in the tree (or possibly
  // keyed object in the case of PROOF) to be processed. The entry argument
  // specifies which entry in the currently loaded tree is to be processed.
  // When processing keyed objects with PROOF, the object is already loaded
  // and is available via the fObject pointer.
  //
  // This function should contain the \"body\" of the analysis. It can contain
  // simple or elaborate selection criteria, run algorithms on the data
  // of the event and typically fill histograms.
  //
  // The processing can be stopped by calling Abort().
  //
  // Use fStatus to set the return value of TTree::Process().
  //
  // The return value is currently not used.

  fReader.SetEntry(entry);

  //rpt distributions
  // if ( *m_jet_pt >= 20e3 and *m_jet_pt <= 40e3) { //pt cut for classic rpt plot, tracks with higher pt usually don't have pileup issues
  //   if (fabs(*m_jet_eta)>=2.4 and fabs(*m_jet_eta)<=4.0) {
  //     if (*m_jet_smallest_deltaR_to_truthcone < 0.3 and *m_jet_pt >10e3) {
  //       hist_rpt_hs->Fill(*m_jet_Rpt);
  //       hist_rpt_hs_hgtd->Fill(*m_jet_Rpt_hgtd);
  //     } else if (*m_jet_smallest_deltaR_to_truthcone > 0.6 and *m_jet_pt > 4e3) {
  //       hist_rpt_pu->Fill(*m_jet_Rpt);
  //       hist_rpt_pu_hgtd->Fill(*m_jet_Rpt_hgtd);
  //     }
  //   }
  // }

  hist_rpt_hs->Fill(*m_jet_Rpt);


  return kTRUE;
}

void JetSelector::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.

}

void JetSelector::Terminate()
{
  // The Terminate() function is the last function to be called during
  // a query. It always runs on the client, it can be used to present
  // the results graphically or save the results to file.

  TFile *output_file = new TFile("jet_output.root", "RECREATE");
  hist_rpt_hs->Write();
  // // drawRptDistributions(hist_rpt_hs, hist_rpt_pu, hist_rpt_hs_hgtd, hist_rpt_pu_hgtd)->Write();
  output_file->Close();
}

//////////////////////////////////////////////////
//////////////////////////////////////////////////
///   code to draw the histograms into canvas  ///
//////////////////////////////////////////////////
//////////////////////////////////////////////////
// TCanvas* JetSelector::drawRptDistributions(TH1F* histo_hs, TH1F* histo_pu, TH1F* histo_hs_hgtd, TH1F* histo_pu_hgtd) {
//   histo_hs->Scale(1./(histo_hs->Integral()));
//   histo_pu->Scale(1./(histo_pu->Integral()));
//   histo_hs_hgtd->Scale(1./(histo_hs_hgtd->Integral()));
//   histo_pu_hgtd->Scale(1./(histo_pu_hgtd->Integral()));
//
//   TCanvas *canvas = new TCanvas("c", "c", 800, 800);
//   canvas->SetLogy();
//   canvas->SetTicks(1,1);
//   TLegend *legend = new TLegend(0.17, 0.7, 0.5, 0.85);
//   legend->SetBorderSize(0);
//   legend->SetTextSize(0.03);
//   legend->SetTextFont(42);
//   legend->SetFillStyle(0);
//   legend->AddEntry(histo_hs, "HS jet, ITk", "p");
//   legend->AddEntry(histo_pu, "PU jet, ITk", "p");
//   legend->AddEntry(histo_hs_hgtd, "HS jet, ITk + HGTD", "p");
//   legend->AddEntry(histo_pu_hgtd, "PU jet, ITk + HGTD ", "p");
//
//   histo_hs->SetAxisRange(1e-4, 1e2,"Y");
//   histo_pu->SetAxisRange(1e-4, 1e2,"Y");
//   histo_hs->SetAxisRange(0, 1.5, "X");
//   histo_pu->SetAxisRange(0, 1.5, "X");
//   histo_hs_hgtd->SetAxisRange(1e-4, 1e2,"Y");
//   histo_pu_hgtd->SetAxisRange(1e-4, 1e2,"Y");
//   histo_hs_hgtd->SetAxisRange(0, 1.5, "X");
//   histo_pu_hgtd->SetAxisRange(0, 1.5, "X");
//
//   histo_hs->Draw("hist");
//   canvas->Modified();
//   canvas->Update();
//   histo_pu->Draw("same hist");
//   canvas->Modified();
//   canvas->Update();
//   histo_hs_hgtd->Draw("same hist");
//   canvas->Modified();
//   canvas->Update();
//   histo_pu_hgtd->Draw("same hist");
//   canvas->Modified();
//   canvas->Update();
//   legend->Draw();
//
//   ATLAS_LABEL(0.2, 0.88, kBlack);
//   myText(0.37, 0.88, kBlack, "Work in progress");
//
//   TString eta_range = Form("%.1f #leq |#eta| #leq %.1f", 2.4, 4.0);
//   TString dataset_buff = m_dataset_name;
//   myText(0.55, 0.6, kBlack, dataset_buff.ReplaceAll("mu", "<#mu>").ReplaceAll(", full", "").Data(), 0.03);
//   myText(0.55, 0.54, kBlack, eta_range.Data(), 0.03);
//   myText(0.55, 0.48, kBlack, "20 < p_{T} < 40 GeV", 0.03);
//
//   return canvas;
// }
