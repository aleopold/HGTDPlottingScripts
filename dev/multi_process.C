// Measure time in a scope
class TimerRAII {
   TStopwatch fTimer;
   std::string fMeta;
public:
   TimerRAII(const char *meta): fMeta(meta) {
      fTimer.Start();
   }
   ~TimerRAII() {
      fTimer.Stop();
      std::cout << fMeta << " - real time elapsed " << fTimer.RealTime() << "s" << std::endl;
   }
};
Int_t multi_process()
{
   // No nuisance for batch execution
   gROOT->SetBatch();
   // Perform the operation sequentially ---------------------------------------
   TChain inputChain("jet_tree"); //name of ttree
   inputChain.Add("/eos/user/a/aleopold/hgtd/results/user.aleopold.hgtd_bbar_mu200_e10gevcut_truthVXtimw.2018_02_23_2241_HGTDHitAnalysis/user.aleopold.13298210.HGTDHitAnalysis*.root");
   TH1F rpt_histo("rpt_histo", "Random Numbers", 50, 0., 1.5);
   {
      TimerRAII t("Sequential read and fit");
      inputChain.Draw("m_jet_Rpt >> rpt_histo");
   }
   // We now go MP! ------------------------------------------------------------
   // TProcPool offers an interface to directly process trees and chains without
   // the need for the user to go through the low level implementation of a
   // map-reduce.
   // We adapt our parallelisation to the number of input files
   const auto nFiles = inputChain.GetListOfFiles()->GetEntries();
   // This is the function invoked during the processing of the trees.
   auto workItem = [](TTreeReader & reader) {
      TTreeReaderValue<Double_t> jet_rpt(reader, "m_jet_Rpt");
      auto partialHisto = new TH1F("outHistoMP", "Random Numbers", 128, -4, 4);
      while (reader.Next()) {
         partialHisto->Fill(*jet_rpt);
      }
      return partialHisto;
   };
   // Create the pool of processes
   TProcPool workers(nFiles);
   // Process the TChain
   {
      TimerRAII t("Parallel execution");
      TH1F *sumHistogram = workers.ProcTree(inputChain, workItem, "multiCore");
   }
   return 0;
}
