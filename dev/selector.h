// This class is derived from the ROOT class TSelector to demonstrate
// the use of PROOF with the ROOT 6 Analysis Workshop from
// http://root.cern.ch/drupal/content/root-6-analysis-workshop
//
// For more information on the TSelector framework see
// $ROOTSYS/README/README.SELECTOR or the ROOT User Manual.
// The following methods are defined in this file:
//    SlaveBegin():   called after Begin(), when on PROOF called only on the
//                    slave servers, a convenient place to create your histograms.
//    Process():      called for each event, in this function you decide what
//                    to read and fill your histograms.
//    Terminate():    called at the end of the loop on the tree,
//                    a convenient place to draw/fit your histograms.
//
// To use this file, try the following session on your Tree T:
//

#ifndef  __SELECTOR_H
#define __SELECTOR_H

#include "TH1F.h"
#include "TSelector.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"

class Selector : public TSelector {
public :

  TH1F *hist_rpt;
  TH1F *hist_pt;
  // Variables used to access and store the data
  TTreeReader tr_jet_tree;                            // The tree reader
  TTreeReaderValue<Double_t> rv_pt;
  TTreeReaderValue<Double_t> rv_rpt;

  Selector(TTree * = nullptr):
    hist_rpt(nullptr),
    hist_pt(nullptr),
    rv_pt(tr_jet_tree, "track_Pt"),
    rv_rpt(tr_jet_tree, "m_jet_Rpt")
    { }

  virtual ~Selector() { }

  virtual void    Init(TTree *tree);
  virtual void    SlaveBegin(TTree *tree);
  virtual Bool_t  Process(Long64_t entry);
  virtual void    Terminate();
  virtual Int_t   Version() const { return 2; }

  ClassDef(Selector,0);
};

#endif //__SELECTOR_H
