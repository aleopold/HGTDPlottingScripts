import ROOT

path = '/data/atlas/aleopold/hgtd/merged.root'

root_file = ROOT.TFile(path, 'read')

tree = root_file.Get("Track")

for event in tree:
  pt = event.track_Pt

root_file.Close()

