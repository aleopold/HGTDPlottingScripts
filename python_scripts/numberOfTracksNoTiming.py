import os
import ROOT

rootfile = "/data/atlas/aleopold/hgtd/merged22.root"


rf = ROOT.TFile(rootfile, "READ")

tree = rf.Get("Track")

n_notime = 0
n_time = 0

for entry in tree:
  if entry.track_time == -5.:
    n_notime += 1
  else:
    n_time += 1

rf.Close()

print n_notime
print n_time
