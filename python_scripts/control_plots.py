import os,sys
import subprocess
from array import array

import ROOT
import AtlasStyle as AS

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
AS.AtlasStyle()

def main():
    root_file_path = "/afs/cern.ch/work/a/aleopold/private/hgtd/simulation-hgtd/run/"
    root_file_name = "HGTDHitAnalysis2018.01.30_16.46.root"
    root_file = root_file_path + root_file_name
    tree_name = "jet_tree"
    control_variables = "m_jet_pt"

    controlPlot(root_file, tree_name, control_variables)

def controlPlot(rootfile_name, tree_name, branch_name):
    rf = ROOT.TFile(rootfile_name, "READ")
    tree = rf.Get(tree_name)

    x = array('f', [0])
    tree.SetBranchAddress(branch_name, x)
    N = tree.GetEntries()
    for i in range(N):
        tree.GetEntry(i)
        print x[0]
    #     break
    # for event in tree:
    #     print event.m_jet_pt
    #     break




if __name__ == "__main__":
    main()
