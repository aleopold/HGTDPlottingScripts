import ROOT 
from array import array

ROOT.gROOT.SetBatch(True)


path = "/afs/cern.ch/work/a/aleopold/private/hgtd/feb_18/run/HGTDHitAnalysis_2018.03.12_18.22.32.294020.root"
root_file = ROOT.TFile(path, 'read')

tree = root_file.Get("track_tree")

x, y = array( 'd' ), array( 'd' )
 
max_event = 1
event_number_buffer = 0
n_event = 0
for event in tree:
  evt_done = False
  if (event.m_eventnumber != event_number_buffer):
    event_number_buffer = event.m_eventnumber
    n_event += 1
  if (n_event > max_event):
    evt_done = True 
  if event.track_time == -5:
    continue
  x.append(event.m_z0_parameter)
  y.append(event.track_time)

  if evt_done:
    graph = ROOT.TGraph(len(x), x, y)
    graph.SetMarkerColor(ROOT.kRed)
    graph.SetMarkerSize(3)

    canvas = ROOT.TCanvas()
    graph.Draw("AP")
    canvas.Print("PLOTS/evtdisplay"+str(event_number_buffer)+".pdf")
    break
