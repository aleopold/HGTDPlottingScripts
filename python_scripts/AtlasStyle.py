import ROOT

def Text(x, y, color, text, textsize):
  l = ROOT.TLatex()
  l.SetNDC()
  l.SetTextSize(textsize) #0.04 good value
  l.SetTextColor(color)
  l.DrawLatex(x,y, text)
  return l

def AtlasLabel( x, y, color, additionalContent)  :
  l = ROOT.TLatex()
  l.SetNDC()
  l.SetTextFont(72)
  l.SetTextColor(color)
  l.DrawLatex(x,y,"ATLAS")
  if (additionalContent) :
    p = ROOT.TLatex()
    p.SetNDC()
    p.SetTextFont(42)
    p.SetTextColor(color)
    p.SetTextSize(0.06)
    p.DrawLatex(x+0.11, y, additionalContent)


def AtlasStyle():
  atlasStyle = ROOT.TStyle("ATLAS","Atlas style")

  icol=0 # WHITE
  atlasStyle.SetFrameBorderMode(icol)
  atlasStyle.SetFrameFillColor(icol)
  atlasStyle.SetCanvasBorderMode(icol)
  atlasStyle.SetCanvasColor(icol)
  atlasStyle.SetPadBorderMode(icol)
  atlasStyle.SetPadColor(icol)
  atlasStyle.SetStatColor(icol)
  #atlasStyle.SetFillColor(icol); # don't use: white fill color for *all* objects

  # set the paper & margin sizes
  atlasStyle.SetPaperSize(20,26)

  # set margin sizes
  atlasStyle.SetPadTopMargin(0.05)
  atlasStyle.SetPadRightMargin(0.05)
  atlasStyle.SetPadBottomMargin(0.16)
  atlasStyle.SetPadLeftMargin(0.16)

  # set title offsets (for axis label)
  atlasStyle.SetTitleXOffset(1.2)
  atlasStyle.SetTitleYOffset(1.3)

  # use large fonts
  #Int_t font=72; # Helvetica italics
  font=42 # Helvetica
  tsize=0.04
  label_name_size=0.05
  atlasStyle.SetTextFont(font)

  atlasStyle.SetTextSize(tsize)
  atlasStyle.SetLabelFont(font,"x")
  atlasStyle.SetTitleFont(font,"x")
  atlasStyle.SetLabelFont(font,"y")
  atlasStyle.SetTitleFont(font,"y")
  atlasStyle.SetLabelFont(font,"z")
  atlasStyle.SetTitleFont(font,"z")

  atlasStyle.SetLabelSize(tsize,"x")
  atlasStyle.SetTitleSize(label_name_size,"x")
  atlasStyle.SetLabelSize(tsize,"y")
  atlasStyle.SetTitleSize(label_name_size,"y")
  atlasStyle.SetLabelSize(tsize,"z")
  atlasStyle.SetTitleSize(label_name_size,"z")

  # use bold lines and markers
  atlasStyle.SetMarkerStyle(20)
  atlasStyle.SetMarkerSize(1.2)
  atlasStyle.SetHistLineWidth(2)
  atlasStyle.SetLineStyleString(2,"[12 12]") # postscript dashes

  # get rid of X error bars
  #atlasStyle.SetErrorX(0.001);
  # get rid of error bar caps
  atlasStyle.SetEndErrorSize(0.)

  # do not display any of the standard histogram decorations
  atlasStyle.SetOptTitle(0)
  #atlasStyle.SetOptStat(1111);
  atlasStyle.SetOptStat(0)
  #atlasStyle.SetOptFit(1111);
  atlasStyle.SetOptFit(0)

  # put tick marks on top and RHS of plots
  atlasStyle.SetPadTickX(1)
  atlasStyle.SetPadTickY(1)

  return atlasStyle
