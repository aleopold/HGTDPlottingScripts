import ROOT

path = "/data/atlas/aleopold/hgtd/merged22.root"

f = ROOT.TFile(path, 'read')

tree = f.Get("Track")

hist = ROOT.TH1F("hist", ";#sigma_t; Events", 100, -1.0, 15.0)

for entry in tree:
  hist.Fill(entry.m_sigma_t)

c = ROOT.TCanvas()
hist.Draw()
c.Print("sigma_t.pdf")

f.Close()
  
