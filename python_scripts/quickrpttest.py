import os
import ROOT

path = "/data/atlas/aleopold/hgtd/user.aleopold.hgtd_bbar_mu200.2018_02_20_1810_HGTDHitAnalysis.176966869/"

for f in os.listdir(path):
  if f.endswith(".root"):
    print "doing", f
    f = ROOT.TFile(path + f)
    t = f.Get("jet_tree")
    for element in t:
      if (element.m_jet_Rpt - element.m_jet_Rpt_hgtd != 0.0):
        print "got one"
    f.Close()

