import os,sys
import subprocess
from array import array

import ROOT
import AtlasStyle as AS

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
AS.AtlasStyle()
ROOT.gROOT.SetStyle("ATLAS")

def main():
    root_file_path = "/afs/cern.ch/work/a/aleopold/private/hgtd/simulation-hgtd/run/"
    root_file_name = "HGTDHitAnalysis2018.01.30_16.46.root"
    root_file = root_file_path + root_file_name

    # rptHistogram(root_file)
    rptHistogramWithHGTD(root_file)


def rptHistogram(root_file):
    """
    Draw the histogram for the RpT variable separated in HS and PU without the HGTD information
    """
    f = ROOT.TFile(root_file)
    jet_tree = f.Get("jet_tree")

    hist_HS = ROOT.TH1F("RpT_HS", ";R_{pT};a.u.", 150, 0, 2)
    hist_HS.SetLineColor(ROOT.kRed)
    hist_PU = ROOT.TH1F("RpT_PU", ";R_{pT};a.u.", 150, 0, 2)
    hist_PU.SetLineColor(ROOT.kBlue)

    for jet in jet_tree:
        if jet.m_is_truth_jet == 1: #truth matched jets are flagged with 1
            hist_HS.Fill(jet.m_jet_Rpt) #the branch called "m_jet_RpT" holds the stored RpT value
        elif jet.m_is_truth_jet == 0: #PU jets are matched with 0
            hist_PU.Fill(jet.m_jet_Rpt)

    canvas = ROOT.TCanvas("c", "c", 800, 800)
    canvas.SetLogy()
    canvas.SetTicks(1,1) #include ticks for the right and top side of the plot

    legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.04)
    legend.SetFillStyle(0)
    legend.AddEntry(hist_HS, "HS jet", "l")
    legend.AddEntry(hist_PU, "PU jet", "l")

    hist_HS.Draw()
    hist_PU.Draw("same")
    legend.Draw()
    canvas.Print("PLOTS/rpt.pdf")
    canvas.Print("PLOTS/rpt.png")


def rptHistogramWithHGTD(root_file):
    """
    Draw the histogram for the RpT variable separated in HS and PU including the HGTD information, to see what kind of improvement the HGTD timing informatin can bring.
    """
    f = ROOT.TFile(root_file)
    jet_tree = f.Get("jet_tree")
    track_tree = f.Get("track_tree")

    jet_eventnumber = array('i', [0])
    jet_id = array('i', [0])
    jet_Rpt = array('f', [0])
    jet_is_truth = array('i', [0])
    track_layers_bit = array('i', [0])
    track_eventnumber = array('i', [0])
    track_jet_id = array('i', [0])
    # track_id = array('i', [0])

    jet_tree.SetBranchAddress("m_eventnumber", jet_eventnumber)
    jet_tree.SetBranchAddress("m_jetID", jet_id)
    jet_tree.SetBranchAddress("m_jet_Rpt", jet_Rpt)
    jet_tree.SetBranchAddress("m_is_truth_jet", jet_is_truth)
    track_tree.SetBranchAddress("m_track_hits_layers_bit", track_layers_bit)
    track_tree.SetBranchAddress("m_eventnumber", track_eventnumber)
    track_tree.SetBranchAddress("m_jetID", track_jet_id)
    # track_tree.SetBranchAddress("m_trackID", track_id)

    hist_HS = ROOT.TH1F("RpT_HS", ";R_{pT};a.u.", 150, 0, 2)
    hist_HS.SetLineColor(ROOT.kRed)
    hist_PU = ROOT.TH1F("RpT_PU", ";R_{pT};a.u.", 150, 0, 2)
    hist_PU.SetLineColor(ROOT.kBlue)

    Nj = jet_tree.GetEntries()
    Nt = track_tree.GetEntries()
    for i in range(Nj): #loop over jets
        ###remove jets that don't have enough hits
        ###TODO MIGHT WANT TO CHANGE THE DEFINITION OF ACCEPTED JETS
        jet_tree.GetEntry(i)
        tracks_are_accepted = False

        for j in range(Nt): #loop over tracks
            track_tree.GetEntry(j)
            if track_eventnumber[0] == jet_eventnumber[0] and track_jet_id[0] == jet_id[0]: #look only at tracks that are associated to the jet
                if track_layers_bit[0] == 15: #accept only tracks that have 4 hits
                    tracks_are_accepted = True
                else:
                    tracks_are_accepted = False
        if jet_is_truth[0] == 1 and tracks_are_accepted: #truth matched jets are flagged with 1
            hist_HS.Fill(jet_Rpt[0]) #the branch called "m_jet_RpT" holds the stored RpT value
            print "accept"
        elif jet_is_truth[0] == 0 or (jet_is_truth[0] == 1 and not tracks_are_accepted): #PU jets are matched with 0
            hist_PU.Fill(jet_Rpt[0])
            print "nope"


    canvas = ROOT.TCanvas("c", "c", 800, 800)
    canvas.SetLogy()
    canvas.SetTicks(1,1) #include ticks for the right and top side of the plot

    legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.04)
    legend.SetFillStyle(0)
    legend.AddEntry(hist_HS, "HS jet", "l")
    legend.AddEntry(hist_PU, "PU jet", "l")

    hist_HS.Draw()
    hist_PU.Draw("same")
    legend.Draw()
    canvas.Print("PLOTS/rpt_HGTD.pdf")
    canvas.Print("PLOTS/rpt_HGTD.png")


if __name__ == "__main__":
    main()
