import ROOT

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptFit(111)

# path = "/afs/cern.ch/work/a/aleopold/private/hgtd/feb_18/run/HGTDHitAnalysis_2018.02.08_13.29.15.root"
# path = "/afs/cern.ch/work/a/aleopold/private/hgtd/feb_18/run/HGTDHitAnalysis2018.02.12_11.17.47.root"
path = "/afs/cern.ch/work/a/aleopold/private/hgtd/feb_18/run/merged2018.02.14_15.45.59.root"



rootfile = ROOT.TFile(path, "READ")

tree = rootfile.Get("Track")

# hist = ROOT.TH1F("timin_res", ";t_{muon}-t_{vertex} [ns]; Entries", 100, 0.25, 0.6)
hist = ROOT.TH1F("track_time", ";t_{track}[ns]; Entries", 100, -0.6, 1.5)
# hist2 = ROOT.TH1F("track_time_res", ";t_{res}[ns]; Entries", 100, 0.1, 0.8)

for entry in tree:
    # hist.Fill(entry.track_time_res)
    # if (entry.track_Pt > 1000):
    if (entry.track_time > -5):
        # hist.Fill(entry.track_time)
        hist.Fill(entry.track_time)
    # if (entry.track_time_res > -5):
    #     hist2.Fill(entry.track_time_res)

c = ROOT.TCanvas()
c.Divide(2,1)
c.cd(1)
hist.Draw("PE1")
hist.Fit("gaus")

c.cd(2)

hist2.Draw("PE1")
hist2.Fit("gaus")
# hist.Fit("gaus", "", "",0.1,0.7)
c.Print("time_bbar.pdf")

rootfile.Close()
