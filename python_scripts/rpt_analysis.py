import os,sys
import subprocess
from array import array

import ROOT
import AtlasStyle as AS

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(0)
AS.AtlasStyle()
ROOT.gROOT.SetStyle("ATLAS")

def main():
    root_file_path = "/afs/cern.ch/work/a/aleopold/private/hgtd/simulation-hgtd/run/"
    root_file_name = "HGTDHitAnalysis2018.01.30_16.46.root"
    root_file = root_file_path + root_file_name

    rptHistogram(root_file)
    rptHistogramWithHGTD(root_file)


def rptHistogram(root_file):
    """
    Draw the histogram for the RpT variable separated in HS and PU without the HGTD information
    """
    f = ROOT.TFile(root_file)
    jet_tree = f.Get("jet_tree")

    hist_HS = ROOT.TH1F("RpT_HS", ";R_{pT};a.u.", 150, 0, 2)
    hist_HS.SetLineColor(ROOT.kRed)
    hist_PU = ROOT.TH1F("RpT_PU", ";R_{pT};a.u.", 150, 0, 2)
    hist_PU.SetLineColor(ROOT.kBlue)

    for jet in jet_tree:
        if abs(jet.m_jet_eta) < 2.4: #only consider HGTD acceptance region
            continue
        if jet.m_is_truth_jet == 1: #truth matched jets are flagged with 1
            hist_HS.Fill(jet.m_jet_Rpt) #the branch called "m_jet_RpT" holds the stored RpT value
        elif jet.m_is_truth_jet == 0: #PU jets are matched with 0
            hist_PU.Fill(jet.m_jet_Rpt)

    canvas = ROOT.TCanvas("c", "c", 800, 800)
    canvas.SetLogy()
    canvas.SetTicks(1,1) #include ticks for the right and top side of the plot

    legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.04)
    legend.SetFillStyle(0)
    legend.AddEntry(hist_HS, "HS jet", "l")
    legend.AddEntry(hist_PU, "PU jet", "l")

    hist_HS.Draw()
    hist_PU.Draw("same")
    legend.Draw()
    canvas.Print("PLOTS/rpt_largeEta.pdf")
    canvas.Print("PLOTS/rpt_largeEta.png")


def rptHistogramWithHGTD(root_file):
    """
    Draw the histogram for the RpT variable separated in HS and PU including the HGTD information, to see what kind of improvement the HGTD timing informatin can bring.
    """
    f = ROOT.TFile(root_file)
    jet_tree = f.Get("jet_tree")
    track_tree = f.Get("track_tree")
    hit_tree = f.Get("hit_tree")

    hist_HS = ROOT.TH1F("RpT_HS", ";R_{pT};a.u.", 150, 0, 2)
    hist_HS.SetLineColor(ROOT.kRed)
    hist_PU = ROOT.TH1F("RpT_PU", ";R_{pT};a.u.", 150, 0, 2)
    hist_PU.SetLineColor(ROOT.kBlue)

    for jet in jet_tree:
        if abs(jet.m_jet_eta) < 2.4: #only consider HGTD acceptance region
            continue
        #remove jets that don't have enough hits
        #TODO MIGHT WANT TO CHANGE THE DEFINITION OF ACCEPTED JETS
        jet_eventnumber = jet.m_eventnumber
        jet_id = jet.m_jetID
        tracks_are_accepted = False
        for track in track_tree:
            if track.m_eventnumber == jet_eventnumber and track.m_jetID == jet_id: #look only at tracks that are associated to the jet
                if track.m_track_hits_layers_bit == 15: #accept only tracks that have 4 hits
                    tracks_are_accepted = True
                else:
                    tracks_are_accepted = False
        if jet.m_is_truth_jet == 1 and tracks_are_accepted: #truth matched jets are flagged with 1
            hist_HS.Fill(jet.m_jet_Rpt) #the branch called "m_jet_RpT" holds the stored RpT value
            print "accept"
        elif jet.m_is_truth_jet == 0 or (jet.m_is_truth_jet == 1 and not tracks_are_accepted): #PU jets are matched with 0
            hist_PU.Fill(jet.m_jet_Rpt)
            print "nope"

    canvas = ROOT.TCanvas("c", "c", 800, 800)
    canvas.SetLogy()
    canvas.SetTicks(1,1) #include ticks for the right and top side of the plot

    legend = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
    legend.SetBorderSize(0)
    legend.SetTextSize(0.04)
    legend.SetFillStyle(0)
    legend.AddEntry(hist_HS, "HS jet", "l")
    legend.AddEntry(hist_PU, "PU jet", "l")

    hist_HS.Draw()
    hist_PU.Draw("same")
    legend.Draw()
    canvas.Print("PLOTS/rpt_largeEta_HGTD.pdf")
    canvas.Print("PLOTS/rpt_largeEta_HGTD.png")


if __name__ == "__main__":
    main()
