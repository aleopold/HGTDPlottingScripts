import ROOT
from array import array

ROOT.gROOT.SetBatch(True)

class Track:
    '''
    Stores the information of a track in one object.
    '''
    def __init__(self, z, t, tag, delta_r, pt):
        self.z = z #z0 parameter
        self.t = t #time assigned to the track
        self.tag = tag #int (0,1) to distinguish tracks that survive a certain cut from those that don't
        self.dr = delta_r #deltaR distance to closest truth particle
        self.pt = pt #pt of the track

class TrackSelection:
    '''
    Stores all tracks of one events and labels them according to specific selections.
    '''
    def __init__(self):
        self.tracks = [] #stores all tracks of the event
        self.max_selected_pt_track = Track(0, 0, 0, 0, 0)
        self.max_event_pt_track = Track(0, 0, 0, 0, 0)

    def addTrack(self, z, t, tag, delta_r, pt):
        self.tracks.append(Track(z, t, tag, delta_r, pt))
        if tag == 1 and pt > self.max_selected_pt_track.pt:
            self.max_selected_pt_track = Track(z, t, tag, delta_r, pt)
        if pt > self.max_event_pt_track.pt:
            self.max_event_pt_track = Track(z, t, tag, delta_r, pt)

    def emptyTracks(self):
        self.tracks = []
        self.max_selected_pt_track = Track(0, 0, 0, 0, 0)
        self.max_event_pt_track = Track(0, 0, 0, 0, 0)

    def combineToTGraph(self, tag):
        x = array('d')
        y = array('d')
        for track in self.tracks:
            if track.tag == tag:
                x.append(track.z)
                y.append(track.t)
        if len(x) == 0:
          emptrygr = ROOT.TGraph()
          return emptrygr.Clone()
        g = ROOT.TGraph(len(x), x, y)
        return g.Clone()

    def combineTruthTGraph(self):
        x = array('d')
        y = array('d')
        for track in self.tracks:
            if track.dr < 0.01:
                x.append(track.z)
                y.append(track.t)
        if len(x) == 0:
          emptrygr = ROOT.TGraph()
          return emptrygr.Clone()
        g = ROOT.TGraph(len(x), x, y)
        return g.Clone()

    def drawPointsFromTrack(self, track):
        x = array('d')
        y = array('d')
        x.append(track.z)
        y.append(track.t)
        if len(x) == 0:
          emptrygr = ROOT.TGraph()
          return emptrygr.Clone()
        g = ROOT.TGraph(len(x), x, y)
        return g.Clone()


    def printToPlot(self, pvx_position, eventnumber, selection):
        c = ROOT.TCanvas()
        signal_graph = self.combineToTGraph(1)
        signal_graph.GetXaxis().SetTitle("z0 [mm]")
        signal_graph.GetYaxis().SetTitle("t [ns]")
        signal_graph.SetMarkerColor(ROOT.kRed)
        signal_graph.SetMarkerStyle(8)
        signal_graph.SetMarkerSize(1.2)
        background_graph = self.combineToTGraph(0)
        background_graph.GetXaxis().SetTitle("z0 [mm]")
        background_graph.GetYaxis().SetTitle("t [ns]")
        background_graph.SetMarkerColor(ROOT.kBlue)
        background_graph.SetMarkerStyle(8)
        background_graph.SetMarkerSize(1.2)
        truth_graph = self.combineTruthTGraph()
        truth_graph.GetXaxis().SetTitle("z0 [mm]")
        truth_graph.GetYaxis().SetTitle("t [ns]")
        truth_graph.SetMarkerColor(ROOT.kOrange)
        truth_graph.SetMarkerStyle(8)
        truth_graph.SetMarkerSize(2)
        truth_graph.SetLineWidth(1)
        top_selected_pt = self.drawPointsFromTrack(self.max_selected_pt_track)
        top_selected_pt.SetMarkerStyle(30)
        top_selected_pt.SetMarkerSize(3)
        top_event_pt = self.drawPointsFromTrack(self.max_event_pt_track)
        top_event_pt.SetMarkerStyle(40)
        top_event_pt.SetMarkerSize(3)

        if truth_graph.GetN() != 0:
            truth_graph.Draw('AP')
            background_graph.Draw('P')
            signal_graph.Draw('P')
        else:
            if background_graph.GetN() != 0:
                background_graph.Draw('AP')
                signal_graph.Draw('P')
            else:
                signal_graph.Draw('AP')
                # background_graph.Draw('P')
        top_event_pt.Draw('P')
        top_selected_pt.Draw('P')
        c.Modified()
        line = ROOT.TLine(pvx_position, c.GetUxmin(), pvx_position, c.GetUxmax())
        line.Draw('same')
        c.Print('PLOTS/eventDisplay' + str(eventnumber) + selection + '.pdf')
        c.Print('PLOTS/eventDisplay' + str(eventnumber) + selection + '.png')


def main():
    path = '/afs/cern.ch/work/a/aleopold/private/hgtd/feb_18/run/HGTDHitAnalysis_2018.03.13_16.12.41.389867.root'
    root_file = ROOT.TFile(path, 'read')

    ope = root_file.Get('once_per_event_tree')
    pvx_dict = {}
    for evt in ope:
      pvx_dict[evt.m_eventnumber] = evt.m_vertex_z

    tree = root_file.Get('track_tree')

    primary_vertex_selection = TrackSelection()
    delta_z_selection = TrackSelection()

    counter = 0
    maxcounter = 50 #maximum number of plotted events
    eventn = 0
    for evt in tree:
        if eventn != evt.m_eventnumber:
            if counter != 0:
                primary_vertex_selection.printToPlot(pvx_dict[eventn], eventn, 'PV')
                delta_z_selection.printToPlot(pvx_dict[eventn], eventn, 'DZ')
                primary_vertex_selection.emptyTracks()
                delta_z_selection.emptyTracks()
            eventn = evt.m_eventnumber
            counter += 1
            if counter > maxcounter:
                break

        if evt.track_time == -5: #only use tracks with a time assigned
            continue

        primary_vertex_selection.addTrack(evt.m_z0_parameter, evt.track_time, evt.m_track_from_pv, evt.m_track_smallest_DeltaR, evt.track_Pt)
        delta_z_selection.addTrack(evt.m_z0_parameter, evt.track_time, evt.m_track_passes_dz, evt.m_track_smallest_DeltaR, evt.track_Pt)





if __name__ == "__main__":
    main()
