#!/usr/bin/python
import ROOT
from array import array

ROOT.gROOT.SetBatch(True)

def drawEvent(x, y, xpv, ypv, highx, highy, eventnumber, selection, vx_dict):
    grhigh = ROOT.TGraph(len(highx), highx, highy)
    grhigh.SetMarkerColor(ROOT.kBlack)
    grhigh.SetMarkerStyle(5)
    grhigh.SetMarkerSize(1)

    if len(x) != 0:
        gr = ROOT.TGraph(len(x), x, y)
        gr.GetXaxis().SetTitle("z0 [mm]")
        gr.GetYaxis().SetTitle("t [ns]")
        gr.SetMarkerColor(ROOT.kBlue)
        gr.SetMarkerStyle(8)
        gr.SetMarkerSize(1)
    if len(xpv) != 0:
        grpv = ROOT.TGraph(len(xpv), xpv, ypv)
        grpv.GetXaxis().SetTitle("z0 [mm]")
        grpv.GetYaxis().SetTitle("t [ns]")
        grpv.SetMarkerColor(ROOT.kRed)
        grpv.SetMarkerStyle(8)
        grpv.SetMarkerSize(1)
    c = ROOT.TCanvas()
    print vx_dict[eventnumber]


    if len(x) == 0 and len(xpv) != 0:
        grpv.Draw('AP')
        grhigh.Draw('P same')
    elif len(xpv) == 0 and len(x) != 0:
        gr.Draw('AP')
        grhigh.Draw('P same')
    elif len(x) == 0 and len(xpv) == 0:
        pass
    else:
        gr.Draw('AP')
        grpv.Draw('P same')
        grhigh.Draw('P same')
#    c.Update()
    #draw line resembling primary vertex
    line = ROOT.TLine(vx_dict[eventnumber], c.GetUxmin(), vx_dict[eventnumber], c.GetUxmax())
    line.Draw('same')
    c.Update()
    c.Print('PLOTS/eventDisplay' + str(eventnumber) + selection + '.pdf')
    c.Print('PLOTS/eventDisplay' + str(eventnumber) + selection + '.png')

path = '/afs/cern.ch/work/a/aleopold/private/hgtd/feb_18/run/HGTDHitAnalysis_2018.03.13_14.50.15.534617.root'
root_file = ROOT.TFile(path, 'read')
tree = root_file.Get('track_tree')

ope = root_file.Get('once_per_event_tree')
pvx_dict = {}
for evt in ope:
  pvx_dict[evt.m_eventnumber] = evt.m_vertex_z
#print pvx_dict


x = array('d')
y = array('d')
x_pv = array('d')
y_pv = array('d')
x_dz = array('d')
y_dz = array('d')
x_highpt = array('d', [0.0])
y_highpt = array('d', [0.0])

counter = 0
maxcounter = 50
eventn = 0
highestpt = 0.0
for evt in tree:
    if eventn != evt.m_eventnumber:
        if counter != 0:
            drawEvent(x, y, x_pv, y_pv, x_highpt, y_highpt, eventn, "PV", pvx_dict)
            drawEvent(x, y, x_dz, y_dz, x_highpt, y_highpt, eventn, "DZ", pvx_dict)
            x = array('d', [])
            y = array('d', [])
            x_pv = array('d', [])
            y_pv = array('d', [])
            x_dz = array('d', [])
            y_dz = array('d', [])
            x_highpt = array('d', [0.0])
            y_highpt = array('d', [0.0])
            highestpt = 0.0
        eventn = evt.m_eventnumber
        counter += 1
        if counter > maxcounter:
            break
    else:
        pass
    if evt.track_time == -5:
        continue
    if evt.m_track_from_pv == 1:
        x_pv.append(evt.m_z0_parameter)
        y_pv.append(evt.track_time)
    else:
        x.append(evt.m_z0_parameter)
        y.append(evt.track_time)
    if evt.m_track_passes_dz == 1:
        x_dz.append(evt.m_z0_parameter)
        y_dz.append(evt.track_time)
    if evt.track_Pt > highestpt:
        highestpt = evt.track_Pt
        x_highpt[0] = evt.m_z0_parameter
        y_highpt[0] = evt.track_time




print counter

